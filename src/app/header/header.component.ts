import { Component, OnInit, ViewChild } from '@angular/core';
import { SearchService } from '../services/search.service';
import { Search } from '../data/search';
import { SEARCH } from '../data/searchdata';
import {NgbTypeahead} from '@ng-bootstrap/ng-bootstrap';
import {Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, map, merge} from 'rxjs/operators';
import { CategoriesService } from '../services/categories.service';
import { Categories } from '../data/categories';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  title = '../assets/images/logo/mensome_logo.png';
  store = 'store';
  mobile_covers = 'mobile covers';
  paytm = 'paytm store';
  searchList: Search[];
  @ViewChild('instance') instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();
  categoryList: Categories[];

  constructor(private searchservice: SearchService, private categoryservice: CategoriesService) { }

  ngOnInit() {
    this.searchList = this.searchservice.getSearch();
    this.categoryList = this.categoryservice.getCategories();
  }

    public model: any;

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      merge(this.focus$),
      merge(this.click$.pipe(filter(() => !this.instance.isPopupOpen()))),
      map(term => (term === '' ? this.searchList
        : this.searchList.filter(v => v.searchitem.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );

   formatter = (x: {searchitem: string}) => x.searchitem;

   searchUrl(url) {
      if (url) {
          window.open(url,"_self");
      }
    }

    openNav() {
      document.getElementById("mySidenav").style.width = "291px";
      console.log('open');
    }

    closeNav() {
      document.getElementById("mySidenav").style.width = "0";
      console.log('close');
    }



}

