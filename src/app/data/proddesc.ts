export class ProdDesc {
	productId: string;
	productImage: string;
	productImageAlt: string;
	productName: string;
	newPrice: number;
	oldPrice: number;
	companyLogo: string;
	companyName: string;
	url: string;
	type: string;
	productColor: string;
}