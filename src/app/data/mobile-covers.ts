import { MobileCoverModel } from './mobile-cover-model';
export class mobileCovers{
	id: number;
	brand: string;
	collapse: string;
	model: MobileCoverModel[];
}