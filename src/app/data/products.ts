import { ProdDesc } from './proddesc';
export class Products {
	id:number;
	productCat: string;
	productName: string;
	product: ProdDesc[];
}

export class Jeans {
	id:number;
	productCat: string;
	productName: string;
	product: ProdDesc[];
}