import { Categories } from './categories';

export const CATEGORIES: Categories[] = [
	{
		id: 1,
	 	categoryName:'Accessories',
	 	categoryaccId: 'Accessories',
	 	subcatName: [
	 		{
	 			subcategory: 'Ties, Cufflinks & Pocket square',
	 			subcategoryaccId: 'ties',
	 			subsubCatName: [
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'All',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Neck Tie',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Bow Tie',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Cuff Links',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Pocket Square',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Brooch/Lapel Pin',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Tie Pin',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Combo sets',
	 				},
	 			],
	 		},
	 		{
	 			subcategory: 'Wallets',
	 			subcategoryaccId: 'wallets',
	 			subsubCatName: [
	 				{
	 					subsubicon: 'black-tie',
	 					subsubcategory: 'Ties',
	 				},
	 			],
	 		},
	 		{
	 			subcategory: 'Belts',
	 			subcategoryaccId: 'belts',
	 			subsubCatName: [
	 				{
	 					subsubicon: 'black-tie',
	 					subsubcategory: 'Ties',
	 				},
	 			],
	 		},
	 		{
	 			subcategory: 'Wristwear',
	 			subcategoryaccId: 'wristwear',
	 			subsubCatName: [
	 				{
	 					subsubicon: 'black-tie',
	 					subsubcategory: 'Ties',
	 				},
	 			],
	 		},
	 		{
	 			subcategory: 'Gift sets',
	 			subcategoryaccId: 'giftsets',
	 			subsubCatName: [
	 				{
	 					subsubicon: 'black-tie',
	 					subsubcategory: 'Ties',
	 				},
	 			],
	 		},
	 		{
	 			subcategory: 'Tech accessories',
	 			subcategoryaccId: 'tech',
	 			subsubCatName: [
	 				{
	 					subsubicon: 'black-tie',
	 					subsubcategory: 'Ties',
	 				},
	 			],
	 		},
	 	],
	},
	{
		id: 2,
		categoryName:'Clothing',
		categoryaccId: 'clothing',
		subcatName: [
	 		{
	 			subcategory: 'T shirts',
	 			subcategoryaccId: 'tshirts',
	 			subsubCatName: [
	 				{
	 					subsubicon: 'black-tie',
	 					subsubcategory: 'white shirts',
	 				},
	 			],
	 		},
	 		{
	 			subcategory: 'Shirts',
	 			subcategoryaccId: 'shirts',
	 			subsubCatName: [
	 				{
	 					subsubicon: 'black-tie',
	 					subsubcategory: 'Ties',
	 				},
	 			],
	 		},
	 	],
	},
	{
		id: 3,
		categoryName:'Footwear',
		categoryaccId: 'footwear',
		subcatName: [
	 		{
	 			subcategory: 'Leather shoes',
	 			subcategoryaccId: 'leathershoes',
	 			subsubCatName: [
	 				{
	 					subsubicon: 'black-tie',
	 					subsubcategory: 'Ties',
	 				},
	 			],
	 		},
	 		{
	 			subcategory: 'Sandals & Floaters',
	 			subcategoryaccId: 'sandels',
	 			subsubCatName: [
	 				{
	 					subsubicon: 'black-tie',
	 					subsubcategory: 'Ties',
	 				},
	 			],
	 		},
	 		{
	 			subcategory: 'Flip Flops',
	 			subcategoryaccId: 'flipflops',
	 			subsubCatName: [
	 				{
	 					subsubicon: 'black-tie',
	 					subsubcategory: 'Ties',
	 				},
	 			],
	 		},
	 	],
	},
	{
		id: 4,
		categoryName:'Grooming',
		categoryaccId: 'grooming',
		subcatName: [
	 		{
	 			subcategory: 'Beard',
	 			subcategoryaccId: 'beard',
	 			subsubCatName: [
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'All',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Beard Oil',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Beard Wax',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Beard Wash',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Beard color',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Combo Pack',
	 				},
	 			],
	 		},
	 		{
	 			subcategory: 'Body',
	 			subcategoryaccId: 'body',
	 			subsubCatName: [
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'All',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Face wash',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Face Scrub',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Face Mask',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Body soap',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Body Wash',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Face Cream',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Body Lotion',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Combo Pack',
	 				},
	 			],
	 		},
	 		{
	 			subcategory: 'Face',
	 			subcategoryaccId: 'face',
	 			subsubCatName: [
	 				{
	 					subsubicon: 'black-tie',
	 					subsubcategory: 'Ties',
	 				},
	 			],
	 		},
	 		{
	 			subcategory: 'Hair',
	 			subcategoryaccId: 'hair',
	 			subsubCatName: [
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'All',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Hair Oil',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Hair shampoo',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Hair wax/gel',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Hair Cream/Serum',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Combo Pack',
	 				},
	 			],
	 		},
	 		{
	 			subcategory: 'Shave',
	 			subcategoryaccId: 'shave',
	 			subsubCatName: [
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'All',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Shaving Foam',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Shaving Gel',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Shaving Brush',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'After Shave Lotion',
	 				},
	 				{
	 					subsubicon: 'dot-circle-o',
	 					subsubcategory: 'Combo Pack',
	 				},
	 			],
	 		},
	 	],
	},
	{
		id: 5,
		categoryName:'Fragrance',
		categoryaccId: 'fragrance',
		subcatName: [
	 		{
	 			subcategory: 'Deodrants',
	 			subcategoryaccId: 'deo',
	 			subsubCatName: [
	 				{
	 					subsubicon: 'circle-thin',
	 					subsubcategory: 'Ties',
	 				},
	 			],
	 		},
	 		{
	 			subcategory: 'Perfumes',
	 			subcategoryaccId: 'perfumes',
	 			subsubCatName: [
	 				{
	 					subsubicon: 'circle-thin',
	 					subsubcategory: 'Ties',
	 				},
	 			],
	 		},
	 		{
	 			subcategory: 'Eau De toilette',
	 			subcategoryaccId: 'eau',
	 			subsubCatName: [
	 				{
	 					subsubicon: 'circle-thin',
	 					subsubcategory: 'Ties',
	 				},
	 			],
	 		},
	 	],
	},
	{
		id: 7,
		categoryName:'Travel',
		categoryaccId: 'travel',
		subcatName: [
	 		{
	 			subcategory: 'Bacpack',
	 			subcategoryaccId: 'bacpack',
	 			subsubCatName: [
	 				{
	 					subsubicon: 'plane',
	 					subsubcategory: 'Ties',
	 				},
	 			],
	 		},
	 		{
	 			subcategory: 'Laptop Case',
	 			subcategoryaccId: 'laptopcase',
	 			subsubCatName: [
	 				{
	 					subsubicon: 'plane',
	 					subsubcategory: 'Ties',
	 				},
	 			],
	 		}
	 	],
	},
	{
		id: 8,
		categoryName:'Trending items',
		categoryaccId: 'trendingitems',
		subcatName: [
	 		{
	 			subcategory: 'Trending items',
	 			subcategoryaccId: 'trendingitems',
	 			subsubCatName: [
	 				{
	 					subsubicon: 'black-tie',
	 					subsubcategory: 'Ties',
	 				},
	 			],
	 		}
	 	],
	},

 ];