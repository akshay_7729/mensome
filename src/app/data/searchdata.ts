import { Search } from './search';

export const SEARCH: Search[] = [
	{
		id:1,
		searchitem: 'Jeans',
		url: 'http://www.mensome.in/#/shop/jeans',
	},
	{
		id:2,
		searchitem: 'Mobile Covers',
		url: 'http://www.mensome.in/#/mobile-covers',
	},
	{
		id:3,
		searchitem: 'Watches',
		url: 'http://www.mensome.in/#/shop/watches',
	},
	{
		id:4,
		searchitem: 'Shirts',
		url: 'http://www.mensome.in/#/shop/shirts',
	},
	{
		id:5,
		searchitem: 'Silk Shirts',
		url: 'http://www.mensome.in/#/shop/shirts',
	},
	{
		id:6,
		searchitem: 'Deodrants',
		url: 'http://www.mensome.in/#/shop/jeans',
	},
	{
		id:7,
		searchitem: 'Tie',
		url: 'http://www.mensome.in/#/mobile-covers',
	},
	{
		id:8,
		searchitem: 'Suits',
		url: 'http://www.mensome.in/#/shop/watches',
	},
	{
		id:9,
		searchitem: 'T-shirts',
		url: 'http://www.mensome.in/#/shop/shirts',
	},
	{
		id:10,
		searchitem: 'Shoes',
		url: 'http://www.mensome.in/#/shop/shirts',
	},
	{
		id:11,
		searchitem: 'Bags',
		url: 'http://www.mensome.in/#/shop/watches',
	},
	{
		id:12,
		searchitem: 'Earphones',
		url: 'http://www.mensome.in/#/shop/shirts',
	},
	{
		id:13,
		searchitem: 'Mufflers',
		url: 'http://www.mensome.in/#/shop/shirts',
	},

];