import { subCategory } from './subcategory';

export class Categories {
	id:number;
	categoryName: string;
	categoryaccId: string;
	subcatName: subCategory[];
}