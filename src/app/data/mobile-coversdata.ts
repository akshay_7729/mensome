import { mobileCovers } from './mobile-covers';

export const MOBILECOVERS: mobileCovers[] = [
	{
		id: 1,
		brand:'iphone',
		collapse: 'collapseOne',
		model: [
			{
				id: 1,
				modelname: 'iphone X',
				url: 'url',
			},
			{
				id: 2,
				modelname: 'iphone 8 plus',
				url: 'url',
			},
			{
				id: 3,
				modelname: 'iphone 8',
				url: 'url',
			},
			{
				id: 4,
				modelname: 'iphone 7 plus',
				url: 'url',
			},
			{
				id: 5,
				modelname: 'iphone 7',
				url: 'url',
			},
			{
				id: 6,
				modelname: 'iphone 6s plus',
				url: 'url',
			},
			{
				id: 7,
				modelname: 'iphone 6 plus',
				url: 'url',
			},
			{
				id: 8,
				modelname: 'iphone 6',
				url: 'url',
			},
			{
				id: 9,
				modelname: 'iphone SE',
				url: 'url',
			},
			{
				id: 10,
				modelname: 'iphone 5s',
				url: 'url',
			},
			{
				id: 10,
				modelname: 'iphone 5C',
				url: 'url',
			},
			{
				id: 10,
				modelname: 'iphone 5',
				url: 'url',
			},
		],
	},
	{
		id: 2,
		brand:'Xiaomi',
		collapse: 'collapseTwo',
		model: [
			{
				id: 1,
				modelname: 'Redmi note 5 pro',
				url: 'url',
			},
			{
				id: 2,
				modelname: 'Redmi note 5',
				url: 'url',
			},
			{
				id: 3,
				modelname: 'Redmi 4A',
				url: 'url',
			},
			{
				id: 4,
				modelname: 'Mi A1',
				url: 'url',
			},
			{
				id: 5,
				modelname: 'Mi Max 2',
				url: 'url',
			},
			{
				id: 6,
				modelname: 'Redmi 4',
				url: 'url',
			},
			{
				id: 7,
				modelname: 'Redmi Note 4',
				url: 'url',
			},
			{
				id: 8,
				modelname: 'Redmi Note 3',
				url: 'url',
			},
			{
				id: 9,
				modelname: 'Redmi 3S prime',
				url: 'url',
			},
			{
				id: 10,
				modelname: 'Redmi Mi Max',
				url: 'url',
			},
			{
				id: 11,
				modelname: 'Mi 5',
				url: 'url',
			},
			{
				id: 12,
				modelname: 'Mi 4',
				url: 'url',
			},
			{
				id: 13,
				modelname: 'Mi 4i',
				url: 'url',
			},
		],
	},
	{
		id: 8,
		brand:'Samsung',
		collapse: 'collapseEight',
		model: [
			{
				id: 1,
				modelname: 'Galaxy J7 Pro',
				url: 'url',
			},
			{
				id: 2,
				modelname: 'Galaxy J7 Max',
				url: 'url',
			},
			{
				id: 3,
				modelname: 'Galaxy S8 Plus',
				url: 'url',
			},
			{
				id: 4,
				modelname: 'Galaxy S8',
				url: 'url',
			},
			{
				id: 5,
				modelname: 'Galaxy S7 Edge',
				url: 'url',
			},
			{
				id: 6,
				modelname: 'Galaxy S7',
				url: 'url',
			},
			{
				id: 7,
				modelname: 'Galaxy S6 Edge',
				url: 'url',
			},
			{
				id: 8,
				modelname: 'Galaxy S6',
				url: 'url',
			},
			{
				id: 9,
				modelname: 'Galaxy A9 Pro',
				url: 'url',
			},
			{
				id: 10,
				modelname: 'Galaxy C9 Pro',
				url: 'url',
			},
			{
				id: 11,
				modelname: 'Galaxy C7 Pro',
				url: 'url',
			},
			{
				id: 12,
				modelname: 'Galaxy A5(2016)',
				url: 'url',
			},
			{
				id: 13,
				modelname: 'Galaxy A7(2016)',
				url: 'url',
			},
			{
				id: 14,
				modelname: 'Galaxy A5(2017)',
				url: 'url',
			},
			{
				id: 15,
				modelname: 'Galaxy A7(2017)',
				url: 'url',
			},
			{
				id: 16,
				modelname: 'Galaxy J7 Prime',
				url: 'url',
			},
			{
				id: 17,
				modelname: 'Galaxy J5 Prime',
				url: 'url',
			},
			{
				id: 18,
				modelname: 'Galaxy J7',
				url: 'url',
			},
			{
				id: 19,
				modelname: 'Galaxy J5',
				url: 'url',
			},
			{
				id: 20,
				modelname: 'Galaxy A5',
				url: 'url',
			},
			{
				id: 21,
				modelname: 'Galaxy A8',
				url: 'url',
			},
		],
	},
	{
		id: 15,
		brand:'Motorola',
		collapse: 'collapseFifteen',
		model: [
			{
				id: 1,
				modelname: 'Moto 2g',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'Moto 3g',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'Moto X style',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'Moto X play',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'Motoe E power 3g',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'Moto M',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'Moto Z play',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'Moto 2Z play',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'Moto G5',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'Moto G5 Plus',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'Moto G4 Play',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'Moto G4',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'Moto G4 Plus',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'Moto C',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'Moto C Plus',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'Moto E4 Plus',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'Moto G5S',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'Moto G5S Plus',
				url: 'url',
			},
		],
	},
	{
		id: 3,
		brand:'Oneplus',
		collapse: 'collapseThree',
		model: [
			{
				id: 1,
				modelname: 'Oneplus 5T',
				url: 'url',
			},
			{
				id: 2,
				modelname: 'Oneplus 5',
				url: 'url',
			},
			{
				id: 3,
				modelname: 'Oneplus 3T',
				url: 'url',
			},
			{
				id: 4,
				modelname: 'Oneplus 3',
				url: 'url',
			},
			{
				id: 5,
				modelname: 'Oneplus 2',
				url: 'url',
			},
			{
				id: 6,
				modelname: 'Oneplus X',
				url: 'url',
			},
			{
				id: 7,
				modelname: 'Oneplus 1',
				url: 'url',
			},
		],
	},
	{
		id: 4,
		brand:'Oppo',
		collapse: 'collapseFour',
		model: [
			{
				id: 1,
				modelname: 'F1S',
				url: 'url',
			},
			{
				id: 2,
				modelname: 'F1',
				url: 'url',
			},
			{
				id: 3,
				modelname: 'F1 Plus',
				url: 'url',
			},
			{
				id: 4,
				modelname: 'F3',
				url: 'url',
			},
			{
				id: 5,
				modelname: 'F3 Plus',
				url: 'url',
			},
			{
				id: 6,
				modelname: 'A57',
				url: 'url',
			},
			{
				id: 7,
				modelname: 'R9 Plus',
				url: 'url',
			},
		],
	},
	{
		id: 5,
		brand:'Vivo',
		collapse: 'collapseFive',
		model: [
			{
				id: 1,
				modelname: 'V5',
				url: 'url',
			},
			{
				id: 2,
				modelname: 'V5S',
				url: 'url',
			},
			{
				id: 3,
				modelname: 'V5 Plus',
				url: 'url',
			},
			{
				id: 4,
				modelname: 'Y55L',
				url: 'url',
			},
			{
				id: 5,
				modelname: 'V3 Max',
				url: 'url',
			},
		],
	},
	{
		id: 6,
		brand:'Asus',
		collapse: 'collapseSix',
		model: [
			{
				id: 1,
				modelname: 'Zenfone 3 Max',
				url: 'url',
			},
			{
				id: 2,
				modelname: 'Zenfone Selfie',
				url: 'url',
			},
		],
	},
	{
		id: 7,
		brand:'Google',
		collapse: 'collapseSeven',
		model: [
			{
				id: 1,
				modelname: 'Nexus 5',
				url: 'url',
			},
			{
				id: 2,
				modelname: 'Pixel',
				url: 'url',
			},
			{
				id: 3,
				modelname: 'Pixel XL',
				url: 'url',
			},
			{
				id: 4,
				modelname: 'Pixel 2',
				url: 'url',
			},
			{
				id: 5,
				modelname: 'Pixel 2 XL',
				url: 'url',
			},
		],
	},
	{
		id: 9,
		brand:'Lenovo',
		collapse: 'collapseNine',
		model: [
			{
				id: 1,
				modelname: 'K4 Note',
				url: 'url',
			},
			{
				id: 2,
				modelname: 'K5 Note',
				url: 'url',
			},
			{
				id: 3,
				modelname: 'K6 Note',
				url: 'url',
			},
			{
				id: 4,
				modelname: 'K8 Note',
				url: 'url',
			},
			{
				id: 5,
				modelname: 'K6 Power',
				url: 'url',
			},
			{
				id: 2,
				modelname: 'K8 Plus',
				url: 'url',
			},
			{
				id: 3,
				modelname: 'Vibe K5',
				url: 'url',
			},
			{
				id: 4,
				modelname: 'Vibe K5 Plus',
				url: 'url',
			},
			{
				id: 5,
				modelname: 'Zuk Z1',
				url: 'url',
			},
		],
	},
	{
		id: 10,
		brand:'Nokia',
		collapse: 'collapseTen',
		model: [
			{
				id: 1,
				modelname: 'Nokia 6',
				url: 'url',
			},
		],
	},
	{
		id: 11,
		brand:'Sony',
		collapse: 'collapseEleven',
		model: [
			{
				id: 1,
				modelname: 'Xperia XZ',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'Z5 Premium',
				url: 'url',
			},
		],
	},
	{
		id: 12,
		brand:'HTC',
		collapse: 'collapseTwelve',
		model: [
			{
				id: 1,
				modelname: 'Desire 816',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'Desire 820',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'HTC 830',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'One A9',
				url: 'url',
			},
		],
	},
	{
		id: 13,
		brand:'Coolpad',
		collapse: 'collapseTherteen',
		model: [
			{
				id: 1,
				modelname: 'CoolPad Note 3',
				url: 'url',
			},
		],
	},
	{
		id: 14,
		brand:'Huawei',
		collapse: 'collapseFourteen',
		model: [
			{
				id: 1,
				modelname: 'Honor 8Pro',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'Honor 8',
				url: 'url',
			},
			{
				id: 1,
				modelname: 'Nexus 6p',
				url: 'url',
			},
		],
	},

]