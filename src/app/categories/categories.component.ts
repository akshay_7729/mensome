import { Component, OnInit } from '@angular/core';
import { CategoriesService } from '../services/categories.service';
import { Categories } from '../data/categories';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  categoryList: Categories[];

  constructor(private categoryservice: CategoriesService) { }

  ngOnInit() {
  	this.categoryList = this.categoryservice.getCategories();
  }

}
