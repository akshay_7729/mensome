import { Component, OnInit } from '@angular/core';
import { ProductService } from '../services/product.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-main-grid',
  templateUrl: './main-grid.component.html',
  styleUrls: ['./main-grid.component.scss']
})
export class MainGridComponent implements OnInit {

  constructor(
  	private router: Router, 
  	private activatedRoute: ActivatedRoute, 
  	private meta: Meta
  	) {
  		this.meta.addTag({ name: 'description', content: 'Mensome is a fashion website for men' });
    	this.meta.addTag({ name: 'keywords', content: 'Mensome, Fashion, Men Fashion' });
  	 }

  ngOnInit() {

  }

    redirectPage(value){
    	this.router.navigate(['/shop' ,value]);
 	}

}
