import { Component, OnInit } from '@angular/core';
import { ProductService } from '../services/product.service';
import { Products } from '../data/products';
import { Jeans } from '../data/products';
import { RouterModule, Routes } from '@angular/router';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { PricerangePipe } from '../pipes/pricerange.pipe';
import { PricerangeMaxPipe } from '../pipes/pricerange-max.pipe';
import { SelectBrandPipe } from '../pipes/select-brand.pipe';
import { SelectTypePipe } from '../pipes/select-type.pipe';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {
  productList: Products[];
  jeansList: Jeans[];
  productListShow = [];
  productListNew = [];
  minPrice: number = 0;
  maxPrice: number = 1100;
  productCategory: string; 
  public sort: any[] = [
    {type: "Popular"},
    {type: "Best Selling"}
  ];
  public colors: any[] = [
    {
      id: 1,
      productColor: "Black",
      selected: false,
    },
    {
      id: 2,
      productColor: "Green",
      selected: false,
    },
    {
      id: 3,
      productColor: "Red",
      selected: false,
    }
  ]

  constructor(private productservice: ProductService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  	this.productList = this.productservice.getProducts();
  	this.productListShow = this.productList;
    this.productListNew = this.productListShow;
    this.activatedRoute.params 
      .subscribe(params => { 
      this.productCategory = params.type; 
      if (params.type === 'jeans') { 
      this.productListShow = this.productList.filter(product=> product.productCat == 'Jeans'); 
      console.log(this.productCategory); 
      } else if(params.type === 'shirts') { 
      this.productListShow = this.productList.filter(product=> product.productCat == 'Shirts'); 
      } else if(params.type === 'watches') { 
      this.productListShow = this.productList.filter(product=> product.productCat == 'Watches'); 
      } else if(params.type === 'mensome-products') { 
      this.productListShow = this.productList.filter(product=> product.productCat == 'mensome-products'); 
      }  
    }); 

}


  public filterProducts(): void {
    const filteredProductArray = new Array<any>();
    const activeColors = this.colors.filter(c => c.selected).map(c => c.productColor);
    this.productListShow.forEach(prod => {
        const filteredSubProducts = prod.product.filter(p => activeColors.includes(p.productColor));
         if(filteredSubProducts.length > 0){
             const clonedProduct = Object.assign({}, prod);
             clonedProduct.product = filteredSubProducts;
             filteredProductArray.push(clonedProduct);
         }
    });
    this.productListNew = filteredProductArray;
    console.log(this.productListNew);
  }




}
