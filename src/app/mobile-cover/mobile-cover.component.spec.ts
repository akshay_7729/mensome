import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileCoverComponent } from './mobile-cover.component';

describe('MobileCoverComponent', () => {
  let component: MobileCoverComponent;
  let fixture: ComponentFixture<MobileCoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileCoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileCoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
