import { Component, OnInit } from '@angular/core';
import { MobileCoversService } from '../services/mobile-covers.service';
import { mobileCovers } from '../data/mobile-covers';

@Component({
  selector: 'app-mobile-cover',
  templateUrl: './mobile-cover.component.html',
  styleUrls: ['./mobile-cover.component.scss']
})
export class MobileCoverComponent implements OnInit {

  mobileCoverList: mobileCovers[];

  constructor(private mobilecoverservice: MobileCoversService) { }

  ngOnInit() {
  	this.mobileCoverList = this.mobilecoverservice.getMobileCovers();
  }

}
