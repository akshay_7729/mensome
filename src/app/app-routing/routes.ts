import { Routes } from '@angular/router';
import { HeaderComponent } from '../header/header.component';
import { CategoriesComponent } from '../categories/categories.component';
import { CarouselMainComponent } from '../carousel-main/carousel-main.component';
import { FooterComponent } from '../footer/footer.component';
import { MainGridComponent } from '../main-grid/main-grid.component';
import { BottomBannerComponent } from '../bottom-banner/bottom-banner.component';
import { HomeComponent } from '../home/home.component';
import { ShopComponent } from '../shop/shop.component';
import { MobileCoverComponent } from '../mobile-cover/mobile-cover.component';

export const routes:Routes = [
	{path: '', component: HomeComponent},
	{path: 'shop/:type', component: ShopComponent},
	{path: 'mobile-covers', component: MobileCoverComponent},
	{path: '', redirectTo: '', pathMatch: 'full'}
];