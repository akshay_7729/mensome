import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'selectType'
})
export class SelectTypePipe implements PipeTransform {

    transform(items: any, sel?: any): any {
        return sel ? items.filter(sal => sal.type === sel) : items;
    }

}
