import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pricerangeMax'
})
export class PricerangeMaxPipe implements PipeTransform {

  transform(maxVal: any, maxFil?: any): any {
    console.log('minFil', maxFil);
  		return maxFil
  		 ? maxVal.filter(person => person.newPrice <= maxFil) 
  		 : maxVal;
  }

}
