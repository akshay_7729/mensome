import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pricerange'
})
export class PricerangePipe implements PipeTransform {

  transform(minVal: any, minFil?: any): any {
    console.log('minFil', minFil);
  		return minFil
  		 ? minVal.filter(person => person.newPrice >= minFil) 
  		 : minVal;
  }

}
