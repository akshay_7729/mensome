import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'selectBrand'
})
export class SelectBrandPipe implements PipeTransform {

    transform(check: any, checked?: any): any {
    	console.log('checked', checked);
        return checked ? check.filter(sal => sal.type == checked) : check;
    }

}
