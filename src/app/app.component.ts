import { Component } from '@angular/core';
import { Meta } from '@angular/platform-browser';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  constructor(private meta: Meta, private router: Router){
  	this.meta.addTag({ name: 'title', content: 'Mensome: A place for fashion' });
  	this.meta.addTag({ name: 'description', content: 'Mensome: A place for all your needs related to fashion' });

     this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        (<any>window).ga('set', 'page', event.urlAfterRedirects);
        (<any>window).ga('send', 'pageview');
      }
    });
  }
}
