import { Injectable } from '@angular/core';
import { Categories } from '../data/categories';
import { CATEGORIES } from '../data/categorydata';

@Injectable()
export class CategoriesService {

  constructor() { }

  getCategories(): Categories[]{
  	return CATEGORIES;
  }

}
