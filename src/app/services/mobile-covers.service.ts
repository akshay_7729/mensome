import { Injectable } from '@angular/core';
import { mobileCovers } from '../data/mobile-covers';
import { MOBILECOVERS } from '../data/mobile-coversdata';

@Injectable()
export class MobileCoversService {

  constructor() { }

  getMobileCovers(): mobileCovers[]{
  	return MOBILECOVERS;
  }

}
