import { Injectable } from '@angular/core';
import { Products } from '../data/products';
import { Jeans } from '../data/products';
import { PRODUCTS } from '../data/productsdata';
import { JEANS } from '../data/productsdata';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ProductService {

  constructor() { }

  getProducts(): Products[]{
  	return PRODUCTS;
  }

  getJeans(): Jeans[]{
  	return JEANS;
  }

}
