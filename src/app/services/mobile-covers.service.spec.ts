import { TestBed, inject } from '@angular/core/testing';

import { MobileCoversService } from './mobile-covers.service';

describe('MobileCoversService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MobileCoversService]
    });
  });

  it('should be created', inject([MobileCoversService], (service: MobileCoversService) => {
    expect(service).toBeTruthy();
  }));
});
