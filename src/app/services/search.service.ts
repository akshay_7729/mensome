import { Injectable } from '@angular/core';
import { Search } from '../data/search';
import { SEARCH } from '../data/searchdata';

@Injectable()
export class SearchService {

  constructor() { }

  getSearch(): Search[]{
  	return SEARCH;
  }

}
