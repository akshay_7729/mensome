import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { CategoriesService } from './services/categories.service';
import { ProductService } from './services/product.service';
import { MobileCoversService } from './services/mobile-covers.service';
import { SearchService } from './services/search.service';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { CategoriesComponent } from './categories/categories.component';
import { CarouselMainComponent } from './carousel-main/carousel-main.component';
import { FooterComponent } from './footer/footer.component';
import { MainGridComponent } from './main-grid/main-grid.component';
import { BottomBannerComponent } from './bottom-banner/bottom-banner.component';
import { HomeComponent } from './home/home.component';
import { ShopComponent } from './shop/shop.component';
import { PricerangePipe } from './pipes/pricerange.pipe';

import { FormsModule } from '@angular/forms';
import { PricerangeMaxPipe } from './pipes/pricerange-max.pipe';
import { SelectBrandPipe } from './pipes/select-brand.pipe';
import { SelectTypePipe } from './pipes/select-type.pipe';

import 'hammerjs';
import { MobileCoverComponent } from './mobile-cover/mobile-cover.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { BottomNavComponent } from './bottom-nav/bottom-nav.component';





@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CategoriesComponent,
    CarouselMainComponent,
    FooterComponent,
    MainGridComponent,
    BottomBannerComponent,
    HomeComponent,
    ShopComponent,
    PricerangePipe,
    PricerangeMaxPipe,
    SelectBrandPipe,
    SelectTypePipe,
    MobileCoverComponent,
    BottomNavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule.forRoot(),
  ],
  providers: [ CategoriesService, ProductService, MobileCoversService, SearchService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
